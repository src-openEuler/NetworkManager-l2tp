%if 0%{?fedora} < 36 && 0%{?rhel} < 10
%bcond_with gtk4
%else
%bcond_without gtk4
%endif

Summary:   NetworkManager VPN plugin for L2TP and L2TP/IPsec
Name:      NetworkManager-l2tp
Version:   1.20.10
Release:   2
License:   GPLv2+
URL:       https://github.com/nm-l2tp/NetworkManager-l2tp
Source:    https://github.com/nm-l2tp/NetworkManager-l2tp/releases/download/%{version}/%{name}-%{version}.tar.xz

%global ppp_version %(pkg-config --modversion pppd 2>/dev/null || sed -n 's/^#define\\s*VERSION\\s*"\\([^\\s]*\\)"$/\\1/p' %{_includedir}/pppd/patchlevel.h 2>/dev/null | grep . || echo bad)

BuildRequires: make
BuildRequires: gcc
BuildRequires: glib2-devel
BuildRequires: gtk3-devel
BuildRequires: NetworkManager-libnm-devel >= 1:1.20.0
BuildRequires: libnma-devel >= 1.8.0
BuildRequires: ppp-devel
BuildRequires: libtool gettext
BuildRequires: libsecret-devel
BuildRequires: openssl-devel >= 1:1.1.0
BuildRequires: nss-devel
%if %with gtk4
BuildRequires: libnma-gtk4-devel
%endif

Requires: dbus-common
Requires: NetworkManager >= 1:1.20.0
Requires: ppp = %{ppp_version}
Requires: xl2tpd
Recommends: (libreswan or strongswan)
Recommends: (%{name}-gnome  if gnome-shell)
Recommends: (plasma-nm-l2tp if plasma-desktop)

%global __provides_exclude ^libnm-.*\\.so

%description
This package contains software for integrating L2TP and L2TP over
IPsec VPN support with the NetworkManager.

%package gnome
Summary: NetworkManager VPN plugin for L2TP and L2TP/IPsec - GNOME files
Requires: %{name}%{?_isa} = %{version}-%{release}

%description gnome
This package contains software for integrating L2TP and L2TP over
IPsec VPN support with the NetworkManager (GNOME files).

%prep
%setup -q

%build
if [ ! -f configure ]; then
  autoreconf -fi
fi
%configure \
    --disable-static \
%if %with gtk4
    --with-gtk4 \
%endif
%if 0%{?rhel} == 8
    --enable-libreswan-dh2 \
    --with-nm-ipsec-nss-dir=%{_sysconfdir}/ipsec.d \
%else
    --with-nm-ipsec-nss-dir=%{_sharedstatedir}/ipsec/nss \
%endif
    --with-pppd-plugin-dir=%{_libdir}/pppd/%{ppp_version} \
    --with-dist-version=%{version}-%{release}
make %{?_smp_mflags}

%check
make check

%install
make install DESTDIR=%{buildroot} INSTALL="%{__install} -p"

rm -f %{buildroot}%{_libdir}/NetworkManager/lib*.la
rm -f %{buildroot}%{_libdir}/pppd/%{ppp_version}/*.la

mkdir -p %{buildroot}%{_sysconfdir}/ipsec.d
mkdir -p %{buildroot}%{_sysconfdir}/strongswan/ipsec.d
touch %{buildroot}%{_sysconfdir}/ipsec.d/ipsec.nm-l2tp.secrets
touch %{buildroot}%{_sysconfdir}/strongswan/ipsec.d/ipsec.nm-l2tp.secrets

%find_lang %{name}

%pre
# remove any NetworkManager-l2tp <= 1.2.10 transient ipsec-*.secrets files.
rm -f %{_sysconfdir}/ipsec.d/nm-l2tp-ipsec-*.secrets
rm -f %{_sysconfdir}/strongswan/ipsec.d/nm-l2tp-ipsec-*.secrets
exit 0

%files -f %{name}.lang
%{_libdir}/NetworkManager/libnm-vpn-plugin-l2tp.so
%{_datadir}/dbus-1/system.d/nm-l2tp-service.conf
%{_prefix}/lib/NetworkManager/VPN/nm-l2tp-service.name
%{_libexecdir}/nm-l2tp-service
%{_libdir}/pppd/%{ppp_version}/nm-l2tp-pppd-plugin.so
%ghost %attr(0600 - -) %{_sysconfdir}/ipsec.d/ipsec.nm-l2tp.secrets
%ghost %attr(0600 - -) %{_sysconfdir}/strongswan/ipsec.d/ipsec.nm-l2tp.secrets
%doc AUTHORS README.md NEWS
%license COPYING

%files gnome
%{_libexecdir}/nm-l2tp-auth-dialog
%{_libdir}/NetworkManager/libnm-vpn-plugin-l2tp-editor.so
%{_metainfodir}/network-manager-l2tp.metainfo.xml
%if %with gtk4
%{_libdir}/NetworkManager/libnm-gtk4-vpn-plugin-l2tp-editor.so
%endif

%changelog
* Tue Aug 29 2023 gaoxingwang <gaoxingwang1@huawei.com> - 1.20.10-2
- change the way to get ppp version because ppp 2.5.0 no more provide patchlevel.h

* Fri Jul 21 2023 chenzixuan <chenzixuan@kylinos.cn> - 1.20.10-1
- Upgrade to 1.20.10

* Mon Feb 20 2023 li-long315 <lilong@kylinos.cn> - 1.20.8-1
- Upgrade to 1.20.8

* Mon Aug 01 2022 liweiganga <liweiganga@uniontech.com> - 1.20.4-1
- Initial build

